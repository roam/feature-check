#!/usr/bin/make -f
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

PROG=		feature-check

FCHKDATA_PROG?=	${CURDIR}/fcheck-testdata/target/release/fcheck-testdata
FCHKDATA_DIR?=	${CURDIR}/fcheck-testdata/data

MAN1=		feature-check.1
MAN1GZ=		${MAN1}.gz

INSTALL?=	install
MKDIR?=		mkdir -p
RM?=		rm -f
FALSE?=		false

LOCALBASE?=	/usr/local
PREFIX?=	${LOCALBASE}
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/man/man
DOCSDIR?=	${PREFIX}/share/doc/feature-check
EXAMPLESDIR?=	${DOCSDIR}/examples

BINOWN?=	root
BINGRP?=	wheel
BINMODE?=	555

SHAREOWN?=	root
SHAREGRP?=	wheel
SHAREMODE?=	644

COPY?=		-c
STRIP?=		-s

INSTALL_PROGRAM?=	${INSTALL} ${COPY} ${STRIP} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_SCRIPT?=	${INSTALL} ${COPY} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA?=		${INSTALL} ${COPY} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}

REGEN_PYTHON?=	python3

all:		${PROG} ${MAN1GZ}

clean:
		${RM} ${PROG} ${MAN1GZ}

install:	all
		${MKDIR} ${DESTDIR}${BINDIR}
		${INSTALL_SCRIPT} ${PROG} ${DESTDIR}${BINDIR}
		${MKDIR} ${DESTDIR}${MANDIR}1
		${INSTALL_DATA} ${MAN1GZ} ${DESTDIR}${MANDIR}1
		${MKDIR} ${DESTDIR}${DOCSDIR}
		${INSTALL_DATA} README.md ${DESTDIR}${DOCSDIR}

test-single:	${TEST_PROG}
		echo "Testing ${TEST_PROG}"
		prove t
		echo "Testing ${TEST_PROG} complete"

test-real:	${PROG}
		${MAKE} test-single TEST_PROG="./${PROG}"

test:		test-real

test-all:
		(set -e; for f in \
			perl5/feature-check.pl \
			python/3/feature-check.sh \
			rust/target/debug/feature-check \
			rust/target/release/feature-check \
			; do \
			if [ -f "$$f" ]; then \
				${MAKE} test-single TEST_PROG="./$$f"; \
			else \
				echo "Skipping nonexistent $$f" 1>&2; \
			fi; \
		done)
		@echo 'Testing all the feature-check implementations complete'

test-regen:
		${FCHKDATA_PROG} -o python/tests/unit/data.py -f ${FCHKDATA_DIR}/simple.json -v python/tests/unit/data.py.hbs
		(set -e; cd python; ${REGEN_PYTHON} -m tox -e reformat)
		${FCHKDATA_PROG} -o t/04-simple.t -f ${FCHKDATA_DIR}/simple.json -v t/04-simple.t.hbs
		${FCHKDATA_PROG} -o t/bin/fcheck -f ${FCHKDATA_DIR}/simple.json -v t/bin/fcheck.hbs
		chmod 755 t/bin/fcheck

${PROG}:	perl5/feature-check.pl
		${INSTALL} -m 755 perl5/feature-check.pl ${PROG}

${MAN1GZ}:	${MAN1}
		gzip -c9 ${MAN1} > ${MAN1GZ}.tmp || (${RM} ${MAN1GZ}.tmp; ${FALSE})
		mv ${MAN1GZ}.tmp ${MAN1GZ} || (${RM} ${MAN1GZ} ${MAN1GZ}.tmp; ${FALSE})

.PHONY:		all clean test test-real test-single test-all
