<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# feature-check - query a program for supported features

\[[Home][ringlet-feature-check] | [GitLab][gitlab] | [Python API](api/python.md) | [Rust API][rustdoc] | [Download](download.md) | [PyPI][pypi] | [crates.io][crates-io] | [ReadTheDocs][readthedocs]\]

The `feature-check` tool obtains the list of supported features from
a program via various methods (e.g. running it with the `--features`
command-line option) and allows other programs to check for the presence
and, possibly, versions of specific features.

[rustdoc]: https://docs.rs/feature-check/latest/

## Check for a single feature

Specify a program name and a feature name; the `feature-check` tool will
exit with an exit code of 0 if the program supports it, and a non-zero
exit code if the program doesn't support the feature or something goes
wrong when querying it (e.g. it doesn't support the `--features` option
at all):

``` sh
#!/bin/sh

if feature-check confget ini; then
    # process an INI file with the confget tool
fi
```

If the program needs a different option to display its list of features,
specify it using the `feature-check` tool's `-O` option:

``` sh
[roam@straylight ~]$ feature-check curl AsynchDNS; echo $?
2
[roam@straylight ~]$ feature-check -O--version curl AsynchDNS; echo $?
0
[roam@straylight ~]$
```

To display the version of a feature advertised by the program, use the `-v`
option:

``` sh
[roam@straylight ~]$ feature-check -v timelimit subsecond
1.0
[roam@straylight ~]$
```

If the program's features output is on a line which starts with a marker
other than "Features: ", specify it using the `-P` option.

## Compare a single feature against a version number

After the program name, specify one or more arguments that, when all
concatenated, form a "feature op version" string.  The operation may be
one of:

- `lt`, `<`: the program's feature is at an earlier version
- `le`, `<=`: the program's feature is at an earlier or the same version
- `eq`, `=`: the program's feature is at the specified version
- `ge`, `>=`: the program's feature is at a later or the same version
- `gt`, `>`: the program's feature is at a later version

Here are some examples:

``` sh
[roam@straylight ~]$ feature-check -l feature-check
feature-check   0.1.0
list    1.0
simple  1.0
single  1.0
[roam@straylight ~]$ feature-check feature-check 'simple <= 1.0' && echo yes
yes
[roam@straylight ~]$ feature-check feature-check 'simple lt 1.0a' && echo yes
yes
[roam@straylight ~]$ feature-check feature-check simple gt 1.0a && echo yes
[roam@straylight ~]$ feature-check feature-check simple lt 1.0.beta && echo yes
[roam@straylight ~]$ feature-check feature-check simple gt 1.0.beta && echo yes
yes
```

## List a program's features

Specify the `-l` option and a program name; the `feature-check` tool will
query the program's features and display them all.  The default output
format is one feature per line, name and value separated by a tab.
The `-o` option allows specifying alternative output formats; the only one
supported so far is `json` which outputs the features as a JSON object.

``` sh
[roam@straylight ~]$ feature-check -l timelimit
subsecond       1.0
timelimit       1.9.0
[roam@straylight ~]$ feature-check -l -o json timelimit
{
   "subsecond" : "1.0",
   "timelimit" : "1.9.0"
}
[roam@straylight ~]$
```

## Contact

The `feature-check` library was written by [Peter Pentchev][roam].
It is developed in [a GitLab repository][gitlab]. This documentation is
hosted at [Ringlet][ringlet-feature-check] with a copy at [ReadTheDocs][readthedocs].

[roam]: mailto:roam@ringlet.net "Peter Pentchev"
[gitlab]: https://gitlab.com/ppentchev/feature-check "The feature-check GitLab repository"
[pypi]: https://pypi.org/project/feature-check/ "The feature-check Python Package Index page"
[crates-io]: https://crates.io/crates/feature-check "The feature-check crate on crates.io"
[readthedocs]: https://feature-check.readthedocs.io/ "The feature-check ReadTheDocs page"
[ringlet-feature-check]: https://devel.ringlet.net/misc/feature-check/ "The Ringlet feature-check homepage"
