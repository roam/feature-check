# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

{ pkgs ? import <nixpkgs> {}, perl-ver ? "38", py-ver ? "12" }:
let
  python-name = "python3${py-ver}";
  python = builtins.getAttr python-name pkgs;
  python-pkgs = python.withPackages (p: [ p.click p.pyparsing ] );
  perl-tree-name = "perl5${perl-ver}Packages";
  perl-tree = builtins.getAttr perl-tree-name pkgs;
  perl-pkgs = with perl-tree; [ perl JSONXS TestCommand ];
in pkgs.mkShell {
  buildInputs = [ python python-pkgs perl-pkgs ];
  shellHook = ''
    set -e
    make test-single PYTHON3='python3.${py-ver}' TEST_PROG=./python/3/feature-check.sh
    exit
  '';
}
