<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

feature-check - query a program for supported features
======================================================

The `feature-check` library helps programs obtain the list of supported
features from a program via various methods (e.g. running it with
the `--features` command-line option) and check for the presence and,
possibly, versions of specific features.

List a program's features
-------------------------

Use the default `List` mode of operation in the configuration object,
specify the program name and, possibly, the command-line option to use and
the string that the feature list line should start with:

    use std::error::Error;
    
    use feature_check::defs::{Config, Obtained};
    use feature_check::obtain as fobtain;
    
    fn main() -> Result<(), Box<dyn Error>> {
        let config = Config {
            program: "curl".to_string(),
            option_name: "--version".to_string(),
            ..Config::default()
        };
        match fobtain::obtain_features(&config)? {
            Obtained::Failed(err) => eprintln!("Could not obtain the features: {}", err),
            Obtained::NotSupported => eprintln!("Querying features not supported"),
            Obtained::Features(res) => println!("{} features", res.len()),
        };
        Ok(())
    }

Compare a single feature against a version number
-------------------------------------------------

Once the list of features has been queried, use an expression previously
obtained from the `expr::parse_simple()` function:

    use std::error;
    
    use feature_check::defs::{Config, Obtained};
    use feature_check::expr::{self as fexpr, CalcResult};
    use feature_check::obtain as fobtain;
    
    fn main() -> Result<(), Box<dyn error::Error>> {
        let query = "feature-check >= 0.2";
        let expr = fexpr::parse_simple(&query)?;
        let config = Config {
            program: "feature-check".to_string(),
            ..Config::default()
        };
        match fobtain::obtain_features(&config)? {
            Obtained::Failed(err) => eprintln!(
                "Could not obtain the features for {}: {}",
                config.program, err
            ),
            Obtained::NotSupported => eprintln!("Querying features not supported"),
            Obtained::Features(res) => {
                println!("{} features", res.len());
                match res.contains_key("feature-check") {
                    false => eprintln!("No 'feature-check' in the features list"),
                    true => match expr.get_value(&res)? {
                        CalcResult::Bool(value) => println!("{}: {}", query, value),
                        other => eprintln!("Unexpected result: {:?}", other),
                    },
                };
            }
        };
        Ok(())
    }

For the crate's change history, see
the [changelog](https://gitlab.com/ppentchev/feature-check/-/blob/master/docs/changes.md)
file in the source distribution.
