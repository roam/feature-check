/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */
//! Query a program for its features.
//!
//! The [`obtain_features`] function, when passed
//! a [`Config`] object, will run a program with
//! the appropriate command-line options, analyze its output, and
//! build up a list of the supported features.
//!
//! See the crate-level documentation for sample usage.

use std::process::Command;

use anyhow::Context;

use crate::defs::{Config, ObtainError, Obtained};
use crate::expr::parser;

/// Decode a program's output, assuming it is a valid UTF-8 string.
fn decode_output(program: &str, output: Vec<u8>) -> Result<String, ObtainError> {
    // FIXME: handle non-UTF-8 stuff
    String::from_utf8(output)
        .context("Could not decode a valid UTF-8 string")
        .map_err(|err| ObtainError::DecodeOutput(program.to_owned(), err))
}

/// Run the specified program, analyze its output.
///
/// See the crate-level documentation for sample usage.
///
/// # Errors
///
/// Will return [`Obtained::Failed`] if the program cannot be executed.
/// Will propagate errors from decoding the program's output as UTF-8 lines.
#[inline]
#[allow(clippy::module_name_repetitions)]
pub fn obtain_features(config: &Config) -> Result<Obtained, ObtainError> {
    match Command::new(&config.program)
        .args([&config.option_name])
        .output()
        .context(format!("Could not execute {}", config.program))
    {
        Ok(output) => {
            if output.status.success() {
                decode_output(&config.program, output.stdout)?
                    .lines()
                    .find_map(|line| line.strip_prefix(&config.prefix))
                    .map_or(Ok(Obtained::NotSupported), |line| {
                        Ok(Obtained::Features(
                            parser::parse_features_line(line).map_err(ObtainError::Parse)?,
                        ))
                    })
            } else {
                Ok(Obtained::NotSupported)
            }
        }
        Err(err) => Ok(Obtained::Failed(ObtainError::RunProgram(
            config.program.clone(),
            err,
        ))),
    }
}
